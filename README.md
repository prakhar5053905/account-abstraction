# Steps to reproduce 

Step 1: Get Account address: ```walletFactoryContract.getAddress()``` [Done]

Step 2: Get Wallet contract instance using address from step 1: ```getWalletContract(address)``` [Done]

Step 3: Set Amount and toAddress 

Step 4: Create initCode using ```concat([WALLET_FACTORY_ADDRESS, data])``` and ```walletFactoryContract.interface.encodeFunctionData(
  "createAccount",
  [owners, salt]
);```

Step 5: Generate Signature using `````` and encode the signature

Setp 6: 